#set working directory

#import WT,rnh201,top1 data
WT_rnh201_top1 <- read.table("cosine_similarities_percentages_s.cerevisiae.txt",
                             header = TRUE)
#compared WT to rnh201
WT<-WT_rnh201_top1$WT_YPD
rnh201 <-WT_rnh201_top1$rnh201_delta
top1 <- WT_rnh201_top1$top1_delta
rnh201_top1 <- WT_rnh201_top1$rnh201_delta_top1_delta

#create list of genotypes
genotype_list <- list(WT,rnh201,top1,rnh201_top1)

#write function
cosine_similarity_function <-function(a,b){
  res<-sum(a*b)/sqrt(sum(a**2)*sum(b**2))
  return(res)
}

#build dataframe for heatmap
WT_rnh201_top1_df <- setNames(data.frame(matrix(ncol = 4, nrow = 4)), c("WT", 
                                                                        "rnh201_delta",
                                                                        "top1_delta",
                                                                        "rh201_delta_top1_delta"))
rownames(WT_rnh201_top1_df) <- c("WT", 
                                 "rnh201_delta",
                                 "top1_delta",
                                 "rh201_delta_top1_delta")

#write loop to populate dataframe
for (r in 1:4){
  for (c in 1:4){
    new_cosine_similarity <- cosine_similarity_function(genotype_list[[r]],genotype_list[[c]])
    WT_rnh201_top1_df[r,c] <- new_cosine_similarity
  }
}

WT_rnh201_top1_matrix <- as.matrix(WT_rnh201_top1_df)


#plot heatmap
require(gplots)
require(RColorBrewer)
myBreaks <- c(0.25, 0.35, 0.45,0.55,0.90,0.99, 1)
myCol <- c("khaki1","yellow","firebrick1","red","red3","white")
hm <- heatmap.2(WT_rnh201_top1_matrix, scale="none", Rowv=NA, Colv=NA,
                col = myCol,
                breaks = myBreaks,
                dendrogram = "none",
                margins=c(5,5), cexRow=0.5, cexCol=1.0, key=FALSE,
                trace="none")
