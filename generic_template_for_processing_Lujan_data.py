#import re module
import re
#open files
with open("INPUT_FILE") as file, open("OUTPUT_FILE","w") as outfile1 :
    for line in file:
        #only look at lines that don't have hash-tag in front
        if re.match(r'#',line,flags=0)== None:
            #split line into elements
            spl= line.split("\t")
            if spl[0] != "mit_ref_v2" and spl[6] == "PASS" and spl[CHECK THIS VALUE] == "0\n":
                #make list from a to ...
                #a
                alleles_a = spl[9].split(":")
                a = alleles_a[0]
                #b
                alleles_b = spl[10].split(":")
                b = alleles_b[0]
                #c
                alleles_c = spl[11].split(":")
                c = alleles_c[0]
                #d
                alleles_d = spl[12].split(":")
                d = alleles_d[0]
                #e
                alleles_e = spl[13].split(":")
                e = alleles_e[0]
                #f
                alleles_f = spl[14].split(":")
                f = alleles_f[0]
                #g
                alleles_g = spl[15].split(":")
                g = alleles_g[0]
                #h
                alleles_h = spl[16].split(":")
                h = alleles_h[0]
                #i
                alleles_i = spl[17].split(":")
                i = alleles_i[0]
                #j
                alleles_j = spl[18].split(":")
                j = alleles_j[0]
                #create tree value
                raw_value_topology = a + b + c + d + e + f + g + h + i + j
                topology_value = raw_value_topology.replace("/","")
                #identify topology values and write new line with values if include or exclude
                topology = list(topology_value)
                ancestral_topology = topology[CHECK] + topology[CHECK] + topology[CHECK] + topology[CHECK]
                #ensure mutation not present in ancestor, and if not ensure alternative variant constitutes less than 1% of reads
                if ancestral_topology == "0000":
                    read_counts_a = alleles_a[1].split(",")
                    read_counts_b = alleles_b[1].split(",")
                    a_read_ratio = int(read_counts_a[1])/(int(read_counts_a[0])+int(read_counts_a[1]))
                    b_read_ratio = int(read_counts_b[1])/(int(read_counts_b[0])+int(read_counts_b[1]))
                    if a_read_ratio < 0.01 and b_read_ratio < 0.01:
                        #test whether variant annotated as 0/1 or 1/1
                        variants = [ a , b , c , d , e , f , g , h , i , j]
                        list_haplotypes = [ alleles_a, alleles_b, alleles_c , alleles_d , alleles_e , \
                                        alleles_f , alleles_g , alleles_h , alleles_i , alleles_j ]
                        for i in range(0,CHECK):
                            if variants[i] == "0/1" or variants[i] == "1/1":
                                #assign read ratios
                                read_counts = list_haplotypes[i]
                                read_count_raw_pre_splitting = read_counts[1]
                                read_count_raw = read_count_raw_pre_splitting.split(",")
                                read_count_ref = int(read_count_raw[0])
                                read_count_variant = int(read_count_raw[1])
                                #look at read count for variant
                                if read_count_variant > 20:
                                    if variants[i] == "0/1":
                                        #now look at read ratio
                                        if (0.4 < (read_count_ref/(read_count_ref + read_count_variant)) < 0.6):
                                            new_line = line.rstrip("\n") + "\t" + topology_value + "\n"
                                            outfile1.write(new_line)
                                    elif variants[i] == "1/1":
                                        #now look at read ratio
                                        if (read_count_ref/(read_count_ref + read_count_variant)) < 0.4:
                                            new_line = line.rstrip("\n") + "\t" + topology_value  + "\n"
                                            outfile1.write(new_line)
                                            
with open("OUTPUT_FILE") as file , \
open("mutation_counter_GENOTYPE", "w") as outfile1:
    mutation_dictionary = {}
    for line in file:
        #only look at files that don't have hash-tag in front
        if re.match(r'#',line,flags=0)== None:
            #split line into elements
            spl= line.split("\t")
            length_a = (len(spl[3]))
            length_b = (len(spl[4]))
            if (length_a == length_b):
                variant =  "\tSNP\n"
            elif (length_b - length_a == 1):
                variant =  "\t1_bp_ins\n"
            elif (length_a - length_b == 1):
                variant =  "\t1_bp_del\n"
            elif (1 < (length_b-length_a) < 6):
                variant =  "\t2_5_bp_ins\n"
            elif (1 < (length_a-length_b) < 6):
                variant =  "\t2_5_bp_del\n"
            elif ((length_b-length_a) > 5):
                variant =  "\tgreat_5_bp_ins\n"
            else:
                variant =  "\tgreat_5_bp_del\n"
        if variant in mutation_dictionary:
            mutation_dictionary[variant] = mutation_dictionary[variant] + 1
        else:
            mutation_dictionary[variant] = 1
    for i in mutation_dictionary:
        outfile1.write(str(mutation_dictionary[i])+ "\t" + i )
