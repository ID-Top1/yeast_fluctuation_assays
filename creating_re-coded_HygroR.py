#create codon dictionary
codon_dictionary = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }
#now create dictionary for amino acids
amino_acid_dictionary = {}
for i in codon_dictionary:
    amino_acid = codon_dictionary[i]
    if amino_acid in amino_acid_dictionary:
        amino_acid_dictionary[amino_acid].append(i)
    else:
        amino_acid_dictionary[amino_acid] = [i]
#open hygromycin sequence
with open("HygroR.fa") as file1, \
     open("HygroR_synonymous_variant_no_mononucleotide_repeats_20180129.fa","w") as outfile1:
    sequence = ""
    #import sequence from fasta file
    for line in file1:
        line = line.rstrip()
        if line.startswith(">"):
            fasta_header = line
        else:
            sequence += line
    sequence = sequence.upper()
    sequence = sequence.rstrip()
    #now create dictionary with values for all codons
    codon_number = 0
    sequence_dictionary = {}
    #create dictionary with all nucleotide sequences that could be changed
    alternative_synonymous_possibilities = []
    #assign each codon in the sequence to a dictionary called sequence_dictionary
    for i in range(0,len(sequence),3):
        codon = sequence[i:i+3]
        amino_acid = codon_dictionary[codon]
        sequence_dictionary[codon_number] = sorted(amino_acid_dictionary[amino_acid])
        codon_number += 1
    #now loop through the sequence in combinations of 5, stop 4 amino acids from the end
    #start dictionary for different tandem repeats
    for i in range(0,(len(sequence_dictionary)-4)):
        codons1 = sequence_dictionary[i]
        codons2 = sequence_dictionary[i+1]
        codons3= sequence_dictionary[i+2]
        codons4 = sequence_dictionary[i+3]
        codons5 = sequence_dictionary[i+4]
        for j in range(0,len(codons1)):
            for k in range(0,len(codons2)):
                for l in range(0,len(codons3)):
                    for m in range(0,len(codons4)):
                        for n in range(0,len(codons5)):
                            testing_sequence = codons1[j]+codons2[k]+codons3[l]+codons4[m]+codons5[n]
                            #now see whether any tandem repeats in this sequence
                            for o in range(2,(len(testing_sequence)-2)):
                                #test if start of a repeat
                                if testing_sequence[o:(o+2)] != testing_sequence[(o-2):o]:
                                    #test if a dinucleotide tandem repeat
                                    if (testing_sequence[o:(o+2)] == testing_sequence[(o+2):(o+4)]) and (testing_sequence[o] != testing_sequence[o+1]):
                                        repeat = testing_sequence[o:(o+2)]
                                        number = o
                                        tandem_repeat = ""
                                        while testing_sequence[number:(number+2)] == repeat:
                                            number = number + 2
                                            tandem_repeat += repeat
                                        #exit loop but save tandem repeat and location
                                        #start of testing_sequence is (3*i), start of tandem repeat is this + o. But want to store as integral codon specific units
                                        #initially create coordinates specific to testing_sequence
                                        start_testing_sequence = (o//3)*3
                                        end_testing_sequence = (((o + len(tandem_repeat) - 1)//3)*3) + 3
                                        #now coordinates specific to whole sequence : 3*i is the start of whole sequence, so can just add on start_testing_sequence to this
                                        start = (3*i) + start_testing_sequence
                                        #end is end of codon
                                        end = (3*i) + end_testing_sequence
                                        #test whether tandem repeat in original sequence
                                        if sequence[start:end] != testing_sequence[start_testing_sequence:end_testing_sequence]:
                                            tandem_repeat_in_context = str(start)+ "_" + testing_sequence[start_testing_sequence:end_testing_sequence] + "_" + str(end)
                                            if tandem_repeat_in_context not in alternative_synonymous_possibilities:
                                                alternative_synonymous_possibilities.append(tandem_repeat_in_context)
    #sort possible tandem repeats by length,descending
    alternative_synonymous_possibilities.sort( key = len, reverse = True)
    #create set of values which have already been filled, so as not to duplicate
    converted_coordinates = set()
    final_tandem_repeats = []
    #go back to original sequence, don't use i as may confuse matters
    for p in range(0,len(alternative_synonymous_possibilities)):
        start = int(alternative_synonymous_possibilities[p].split("_")[0])
        tandem_repeat = alternative_synonymous_possibilities[p].split("_")[1]
        end = int(alternative_synonymous_possibilities[p].split("_")[2])
        #define coordinates which have been used, subtract 1 from end as if not will go into next codon
        coordinates = set(list(range(start,(end-1))))
        if not coordinates & converted_coordinates:
            final_tandem_repeats.append(alternative_synonymous_possibilities[p])
            #add coordinates which have been used to list to rule out positions
            converted_coordinates.update(coordinates)
    #create a dictionary with all values then sort
    #create final_coordinates list
    final_coordinates = []
    final_sequence_coordinates = {}
    for q in range(0,len(final_tandem_repeats)):
        start = int(final_tandem_repeats[q].split("_")[0])
        tandem_repeat = final_tandem_repeats[q].split("_")[1]
        end = int(final_tandem_repeats[q].split("_")[2])
        for r in range(0,len(tandem_repeat)):
            nucleotide = tandem_repeat[r]
            nucleotide_position = start+r
            final_coordinates.append(nucleotide_position)
            if nucleotide_position not in final_sequence_coordinates:
                final_sequence_coordinates[nucleotide_position] = nucleotide
    #go back to original sequence
    for s in range(0,len(sequence)):
        if s not in final_sequence_coordinates:
            final_sequence_coordinates[s] = sequence[s]
    #add fasta header to output
    outfile1.write(">HygroR_increased_dinucleotide_repeats_only\n")
    #print off as list
    final_sequence = ""
    for i in range(0,len(sequence)):
        final_sequence += final_sequence_coordinates[i]
    outfile1.write(final_sequence)

#eliminate nonsense mutations
with open("HygroR_synonymous_variant_no_mononucleotide_repeats_20180129.fa") as file2, \
     open("HygroR_synonymous_variant_no_mononucleotide_repeats_no_nonsense_mutations_20180129.fa","w") as outfile2:
    #import sequence from fasta file
    sequence = ""
    for line in file2:
        line = line.rstrip()
        if line.startswith(">"):
            fasta_header = line
        else:
            sequence += line.upper()
    #define stop codons
    stop_codons = amino_acid_dictionary["_"]
    #delete two bp after ATG and stitch together a new sequence with this -2 frameshift
    frameshift_sequence = sequence[0:3] + sequence[5:]
    #now test this new sequence for stop codon, create amino acid sequence
    for i in range(3,len(frameshift_sequence),3):
        codon = frameshift_sequence[i:(i+3)]
        if codon in stop_codons:
            breaker = 0
            #create two new codons
            amino_acid_1 = codon_dictionary[sequence[(i):(i+3)]]
            codons_1 = amino_acid_dictionary[amino_acid_1]
            codons_1.sort()
            amino_acid_2 = codon_dictionary[sequence[(i+3):(i+6)]]
            codons_2 = amino_acid_dictionary[amino_acid_2]
            codons_2.sort()
            #search for solution to this
            for j in range(0,len(codons_1)):
                if breaker == 0:
                    for k in range(0,len(codons_2)):
                        new_possibility = codons_1[j] + codons_2[k]
                        stop_sequence = new_possibility[2:5]
                        if stop_sequence not in stop_codons:
                            sequence = sequence[:i]+ new_possibility + sequence[(i+6):]
                            #activate breaker for earlier on in loop
                            breaker = 1
                            break
            if breaker == 0:
                print("fatal error non-substitutable stop codon at position " + str(i))
    outfile2.write(">HygroR_increased_dinucleotide_repeats_only_no_nonsense_mutations\n" + sequence)
